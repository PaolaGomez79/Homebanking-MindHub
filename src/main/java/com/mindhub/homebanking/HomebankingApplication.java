package com.mindhub.homebanking;

import com.mindhub.homebanking.models.*;
import com.mindhub.homebanking.repositories.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class HomebankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomebankingApplication.class, args);
	}

	@Bean
	public CommandLineRunner initData(ClientRepository clientRepository,
									  AccountRepository accountRepository,
									  TransactionRepository transactionRepository,
									  LoanRepository loanRepository,
									  ClientLoanRepository clientLoanRepository) {
		return (args) -> {

			Client client1 = new Client("Melba", "Morel","melba@mindhub.com");
			Client client2 = new Client("Paola", "Gomez","paomeli79@gmail.com");

			clientRepository.save(client1);
			clientRepository.save(client2);

			LocalDate today = LocalDate.now();
			LocalDate tomorrow = today.plusDays(1);
			LocalDate nextMonth = today.minusDays(1);
			Account account1 = new Account("VIN001",today,5000.0);
			Account account2 = new Account("VIN002",tomorrow,7500.0);
			Account account3 = new Account("VIN003",nextMonth,10000.0);

			client1.addAccount(account1);
			client1.addAccount(account2);
			client2.addAccount(account3);

			accountRepository.save(account1);
			accountRepository.save(account2);
			accountRepository.save(account3);

			clientRepository.save(client1);
			clientRepository.save(client2);

			Transaction transaction1 = new Transaction(TransactionType.CREDIT,2000.0,"Acredito", LocalDateTime.now());
			Transaction transaction2 = new Transaction(TransactionType.DEBIT,-1000.0,"Retiro",LocalDateTime.now());
			Transaction transaction3 = new Transaction(TransactionType.DEBIT,-2000.0,"Retiro",LocalDateTime.now());

			account1.addTransaction(transaction1);
			account2.addTransaction(transaction2);
			account3.addTransaction(transaction3);

			transactionRepository.save(transaction1);
			transactionRepository.save(transaction2);
			transactionRepository.save(transaction3);

			accountRepository.save(account1);
			accountRepository.save(account2);
			accountRepository.save(account3);

			List<Integer> payments1 = List.of(12, 24, 36, 48, 60);
			List<Integer> payments2 = List.of(6, 12, 24);
			List<Integer> payments3 = List.of(6, 12, 24, 36);

			Loan loan1 = new Loan("Hipotecario",500000.0,payments1);
			Loan loan2 = new Loan("Personal",100000.0,payments2);
			Loan loan3 = new Loan("Automotriz",300000.0,payments3);

			loanRepository.save(loan1);
			loanRepository.save(loan2);
			loanRepository.save(loan3);

			ClientLoan clientLoan1 = new ClientLoan(400000.0,60,client1,loan1);
			ClientLoan clientLoan2 = new ClientLoan(50000.0,12,client1,loan2);
			ClientLoan clientLoan3 = new ClientLoan(10000.0,24,client2,loan2);
			ClientLoan clientLoan4 = new ClientLoan(20000.0,36,client2,loan3);

			clientLoanRepository.save(clientLoan1);
			clientLoanRepository.save(clientLoan2);
			clientLoanRepository.save(clientLoan3);
			clientLoanRepository.save(clientLoan4);

			/*clientRepository.save(client1);
			clientRepository.save(client2);*/


		};
	}

}
